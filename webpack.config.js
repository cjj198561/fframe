var path = require('path');
var webpack = require('webpack');
var glob = require('glob');
var _ = require('lodash');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ignore = new webpack.IgnorePlugin(/\.svg$/)
var config = require('./app/config')

var ROOT_PATH = path.resolve(__dirname);
var APP_PATH = path.resolve(ROOT_PATH, 'app');
var TEM_PATH = path.resolve(ROOT_PATH, 'views');

// 获取指定路径下的入口文件
function getEntries(globPath) {
  var files = glob.sync(globPath),
    entries = {};

  files.forEach(function(filepath) {
    // 取倒数第二层(view下面的文件夹)做包名
    var split = filepath.split('/');
    var name = split[split.length - 2];

    entries[name] = filepath;
  });

  return entries;
}

function getEntryAndPlugin() {
  var entries = getEntries(APP_PATH + '/routes/**/index.jsx');
  var entrys = {}
  entrys["vendor"] = [// 把第三方的依赖打成一个包js包
    'babel-polyfill',
    //'es5-shim',
    //'console-polyfill',
    'react',
    'react-dom',
    //'react-router',
    'react-redux',
    'redux',
    'redux-thunk',
    'lodash',
    'isomorphic-fetch',
    'redux-promise-middleware',
  ]

  var plugins = [ignore]
  plugins.push(new webpack.DefinePlugin({
    'process.env': {
      'NODE_ENV': JSON.stringify(config.env)
    }
  }))

  _.map(entries, (filepath, name) => {
    name = name.toLowerCase();
    // 每个页面生成一个entry，如果需要HotUpdate，在这里修改entry
    entrys[name] = filepath;

    // 每个页面生成一个html
    var plugin = new HtmlWebpackPlugin({
      // 标题
      title : name ,
      // 每个html的模版，这里多个页面使用同一个模版
      template: path.resolve(TEM_PATH, 'tpl.html'),
      // 生成出来的html文件名
      filename: name + '.html',
      // 每个html引用的js模块，也可以在这里加上vendor等公用模块
      chunks: [name, "vendor"],
      // 要把script插入到标签里
      inject: 'body'
    });
    plugins.push(plugin);
  })

  return { entrys, plugins }
}

var obj = getEntryAndPlugin()

var webpackConfig = {
  entry: obj.entrys,
  output: {
    path: path.resolve(APP_PATH, 'dist'),
    publicPath: '/', // output.publicPath 表示资源的发布地址，当配置过该属性后，打包文件中所有通过相对路径引用的资源都会被配置的路径所替换。
    filename: 'js/[name].js'
  },
  //enable dev source map
  devtool: 'eval-source-map',
  //enable dev server
  devServer: {
    historyApiFallback: true,
    hot: true,
    inline: true,
    progress: true,
    proxy: {
      '/ref': {
        target: `http://${config.server_host}:${config.server_port}/`,
        changeOrigin: true,
        pathRewrite: {
          '^/ref': '/ref'
        },
      }
    }
  },
  resolve: {
    extensions: ['', '.js', '.jsx'],
    root: APP_PATH
  },
  module: {
    preLoaders: [
      {
        test: /\.jsx?$/,
        loaders: ['eslint'],
        include: APP_PATH
      }
    ],
    loaders: [
      {
        test: /\.jsx?$/,
        // loaders: ['react-hot', 'babel?' + JSON.stringify({presets: ['react', 'es2015', 'stage-0']})],
        loaders: ['react-hot', 'babel'],
        include: APP_PATH,
        exclude: /node_modules/
      },
      {
        test: /\.scss$/,
        loaders: ['style', 'css', 'postcss', 'sass'],
        include: APP_PATH,
        exclude: /node_modules/
      },
      {
        test: /\.(png|jpg|ico)$/,
        loader: 'url-loader?limit=8192&name=images/[name].[ext]'
      }
    ]
  },
  jshint: {
    "esnext": true
  },
  plugins: obj.plugins
}

module.exports = webpackConfig
