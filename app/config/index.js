/* eslint key-spacing:0 spaced-comment:0 */
// ========================================================
// Default Configuration
// ========================================================
const config = {
  env : process.env.NODE_ENV || 'development',
  server_host : '127.0.0.1',
  server_port : '8000'
}

module.exports = config
