/*
 * @file 首页的入口文件
 */
import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import structureStore from '../../store/createStore'
import { injectReducer } from '../../store/reducers'
import HomeRoot from './containers/homeContainers'
import { reducer } from './modules/home'

const store = structureStore()
const MOUNT_NODE = document.getElementById('main')
injectReducer(store, { key: 'home', reducer })

ReactDOM.render(
  <Provider store={store}>
    <HomeRoot />
  </Provider>,
  MOUNT_NODE
)
