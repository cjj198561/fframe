import React, { PropTypes } from 'react'
import _ from 'lodash'
import classes from './style.scss'

class Home extends React.Component {
  componentDidMount() {
    this.props.actions.fetchZen()
  }

  renderLoading() {
    return (this.props.home.fetching)
      ? <div className={classes.loader}>加载中........</div>
      : ''
  }

  render() {
    const { actions, home } = this.props
    const { fetching, data, err } = home
    const { fetchZen, clearZen } = actions
    const child = (item, k) => <h1 key={k}>{item}</h1>
    return (
      <div>
        <div>
          <button className='btn btn-default' onClick={fetchZen}>
            {fetching ? 'Fetching' : 'Fetch'}
          </button>
          <button className='btn btn-default' onClick={clearZen}>Clear</button>
        </div>
        {this.renderLoading()}
        <div>
          {
            err ? <div>{err}</div> : null
          }
          {
            data ? _.map(data, (v, k) => child(v, k)) : null
          }
        </div>
      </div>
    )
  }
}

Home.propTypes = {
  home: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired
}

export default Home
