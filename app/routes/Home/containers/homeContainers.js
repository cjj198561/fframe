import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { actions } from '../modules/home'
import Home from '../components/home.jsx'

const mapDispatchtoProps = (dispatch) => ({
  actions: bindActionCreators(actions, dispatch)
})

const mapStateToProps = (state) => ({
  home: state.home
})

const HomeRoot = connect(mapStateToProps, mapDispatchtoProps)(Home)

export default HomeRoot
