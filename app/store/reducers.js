import { combineReducers } from 'redux';

export const makeRootReducer = (asyncReducers) => {
  if (asyncReducers) {
    return combineReducers({ ...asyncReducers });
  }

  return () => {};
};

export const injectReducer = (store, { key, reducer }) => {
  store.asyncReducers[key] = reducer;
  store.replaceReducer(makeRootReducer(store.asyncReducers));
};
